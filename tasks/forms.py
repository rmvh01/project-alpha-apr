from tasks.models import Task
from django.forms import ModelForm

# model forms here


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "is_complete",
            "project",
            "assignee",
        ]
